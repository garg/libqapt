/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Rohan Garg <rohan16garg@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QAPTWORKER_H
#define QAPTWORKER_H

// Qt includes
#include <QObject>

// LibQApt includes
#include "../../src/globals.h"

namespace QApt {
    class Backend;
    class Transaction;
}

class QAptWorker : public QObject
{
    Q_OBJECT

public:
    QAptWorker(QString mode, QStringList packages, QObject* parent);
    ~QAptWorker();
    void setFrontendCaps(const QApt::FrontendCaps);
    QApt::Transaction *m_trans;
    void run();

Q_SIGNALS:
    void speedChanged(int);
    void progressChanged(int);
    void error(QString);

private:
    QApt::Backend *m_backend;
    QString m_mode;
    int m_lastRealProgress;
    void commitChanges(int, const QStringList &);
public slots:
    void debugProgress(int);
};

#endif // QAPTWORKER_H
