/*
 * Copyright 2013  Rohan Garg <rohan16garg@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QDebug>

// KDE includes
#include <KMessageBox>
#include <KLocale>
#include <KProtocolManager>

#include "qaptworker.h"

#include "../../src/backend.h"
#include "../../src/transaction.h"

QAptWorker::QAptWorker(QString mode, QStringList packages, QObject* parent)
    : QObject(parent)
    , m_backend(new QApt::Backend(this))
    , m_mode(mode)
    , m_lastRealProgress(0)
{
    if (!m_backend->init()) {
        QString details = m_backend->initErrorMessage();

        QString text = i18nc("@label",
                            "The package system could not be initialized, your "
                            "configuration may be broken.");
        QString title = i18nc("@title:window", "Initialization error");

        KMessageBox::detailedError(0, text, details, title);
        exit(-1);
        }

    if (m_mode == "install") {
        commitChanges(QApt::Package::ToInstall, packages);
    } else if (m_mode == "uninstall") {
        commitChanges(QApt::Package::ToRemove, packages);
    } else if (m_mode == "update") {
        qDebug() << "Updating cache";
        m_trans = m_backend->updateCache();
    }

    m_trans->setLocale(setlocale(LC_ALL, 0));
    // Provide proxy/locale to the transaction
    if (KProtocolManager::proxyType() == KProtocolManager::ManualProxy) {
        m_trans->setProxy(KProtocolManager::proxyFor("http"));
    }
    connect (m_trans, SIGNAL(progressChanged(int)), SIGNAL(progressChanged(int)));
    connect(m_trans, SIGNAL(progressChanged(int)), SLOT(debugProgress(int)));
}

QAptWorker::~QAptWorker()
{
}

void QAptWorker::setFrontendCaps(const QApt::FrontendCaps caps)
{
    m_backend->setFrontendCaps(caps);
}

void QAptWorker::commitChanges(int mode, const QStringList& packageStrs)
{
    QApt::PackageList packages;

    QApt::Package *pkg;
    for (const QString &packageStr : packageStrs) {
        pkg = m_backend->package(packageStr);

        if (pkg)
            packages.append(pkg);
        else {
            emit error(QString("The package \"%1\" has not been found among your software sources. "
                               "Therefore, it cannot be installed. ").arg(packageStr));
        }
    }

    m_trans = (mode == QApt::Package::ToInstall) ?
               m_backend->installPackages(packages) :
               m_backend->removePackages(packages);
}

void QAptWorker::run()
{
    m_trans->run();
}

void QAptWorker::debugProgress(int progress)
{
    qDebug() << "Progress " << progress;
}


#include "qaptworker.moc"
