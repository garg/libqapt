/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Rohan Garg <rohan16garg@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "qaptbatchjob.h"
#include "qaptworker.h"

#include <QDebug>

#include "../../src/transaction.h"

QAptBatchJob::QAptBatchJob(QString mode, QStringList pkgList)
    : KJob()
    , m_aptworker(new QAptWorker(mode, pkgList, this))
    , m_lastRealProgress(0)
    , m_mode(mode)
{
    m_trans = m_aptworker->m_trans;
    connect(m_trans, SIGNAL(downloadSpeedChanged(quint64)), SLOT(updateSpeed(quint64)));
    connect(m_aptworker, SIGNAL(progressChanged(int)), SLOT(updateProgress(int)));
}

QAptBatchJob::~QAptBatchJob()
{
}

void QAptBatchJob::start()
{
    if (m_mode == "install") {
        emit description(this, "Installing selected packages");
    } else if (m_mode == "uninstall") {
        emit description(this, "Uninstalling selected packages");
    } else if (m_mode == "update") {
        emit description(this, "Updating package cache");
    }
    m_aptworker->run();
    qDebug() << "Emitting result";
    emitResult();
}

void QAptBatchJob::updateProgress(int progress)
{
    if (progress == 100) {
        --progress;
    }

    m_lastRealProgress = progress;
    setPercent(m_lastRealProgress);
    qDebug() << m_lastRealProgress;
}

void QAptBatchJob::updateSpeed(quint64 speed)
{
    emitSpeed(speed);
}

#include "qaptbatchjob.moc"
