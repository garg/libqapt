/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Rohan Garg <rohan16garg@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef QAPTJOB_H
#define QAPTJOB_H

// KDE Includes
#include <KJob>

class QAptWorker;

namespace QApt {
    class Transaction;
}
class QAptBatchJob : public KJob
{
    Q_OBJECT
public:
    QAptBatchJob(QString mode, QStringList pkgList);
    ~QAptBatchJob();
    void start();

private:
    QAptWorker *m_aptworker;
    QApt::Transaction *m_trans;
    int m_lastRealProgress;
    QString m_mode;

private Q_SLOTS:
    void updateProgress(int);
    void updateSpeed(quint64);
};

#endif // QAPTJOB_H
